﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{

	public TMPro.TMP_Dropdown resDropdown;
	GameObject inGameToggle;

	Resolution[] resolutions;

	void Start()
	{

		inGameToggle = GameObject.Find("Toggle");

        if(Screen.fullScreen == false)
        {
            inGameToggle.GetComponent<Toggle>().isOn = false;
        }
        else
        {
            inGameToggle.GetComponent<Toggle>().isOn = true;
        }
		

		resolutions = Screen.resolutions.Select(resolution => new Resolution { width = resolution.width, height = resolution.height }).Distinct().ToArray();

        resDropdown.ClearOptions();

		List<string> resolution_options = new List<string>();
	
		int currentResolutionIndex = 0;
		for(int i = 0; i < resolutions.Length; i++)
		{    
                string option = resolutions[i].width + "x" + resolutions[i].height;
                resolution_options.Add(option);

                if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
                {
                    currentResolutionIndex = i;
                } 
		}

		resDropdown.AddOptions(resolution_options);
		resDropdown.value = currentResolutionIndex;
		resDropdown.RefreshShownValue();
	}
		

	public void SetResolution(int resolutionIndex)
	{
		Resolution res = resolutions[resolutionIndex];
		Screen.SetResolution(res.width, res.height, Screen.fullScreen);
	}

	public void SetVolume(float volume)
	{
        AudioListener.volume = volume;
	}

	public void SetFullscreen(bool isFullscreen)
	{
		Screen.fullScreen = isFullscreen;
	}

}
