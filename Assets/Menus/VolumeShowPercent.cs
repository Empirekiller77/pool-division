﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class VolumeShowPercent : MonoBehaviour
{
    private TextMeshProUGUI percentageText;

	
	void Start()
	{
		this.percentageText = GetComponent<TMPro.TextMeshProUGUI>();       
	}

	
	public void textUpdate(float value)
	{
		if(this.percentageText == null)
        { 
			this.percentageText = GetComponent<TMPro.TextMeshProUGUI>();
		}

		this.percentageText.text = Mathf.RoundToInt(value * 100) + "%";
		
	}
}
