﻿using UnityEngine;

public class balls_sound_controller : MonoBehaviour
{
    public AudioClip[] audioClips = new AudioClip[4];

    private AudioSource audioSource;

    private void Start()
    {
        this.audioSource = this.GetComponent<AudioSource>();
    }

    internal void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("BallFilter"))
        {
            //balls hitting each other clip
            audioSource.clip = audioClips[0];
            audioSource.Play();
        }

        if (collision.gameObject.tag == "Player")
        {
            //hitting ball clip
            audioSource.clip = audioClips[2];
            audioSource.Play();
        }

        if (collision.gameObject.tag == "TableBarrier")
        {
            //hitting ball clip
            audioSource.clip = audioClips[3];
            audioSource.Play();
        }
    }
}
