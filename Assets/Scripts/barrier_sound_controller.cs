﻿using UnityEngine;

public class barrier_sound_controller : MonoBehaviour
{
    private AudioSource audioSource;

    // Start is called before the first frame update
    internal void Start()
    {
        this.audioSource = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    internal void Update()
    {
    }

    internal void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //car hitting wall
            audioSource.Play();
        }
    }
}
