﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class change_mat : MonoBehaviour
{
    public Material[] material;
    Renderer rend;
    public GameObject[] p1;
    public GameObject[] p2;
    private GameObject GameScoreUI;
    private GameObject ChangeColorUI;
    private Toggle ToggleP1;
    private Toggle ToggleP2;
    private Button PlayButton;
   
    public void Start()
    {
        ToggleP1 = GameObject.Find("ReadyP1").GetComponent<Toggle>();
        ToggleP2 = GameObject.Find("ReadyP2").GetComponent<Toggle>();
        PlayButton = GameObject.Find("PlayButton").GetComponent<Button>();
        ChangeColorUI = GameObject.Find("Canvas Change Color"); 
        ChangeColorUI.SetActive(true);
        Time.timeScale = 0f;
        GameScoreUI = GameObject.Find("Game Score Canvas");
        GameScoreUI.GetComponent<Canvas>().enabled = false;
    }

    public void OnClickOrange_p1()
    {
        for(int i = 0; p1.Length > i; i++)
        {
            rend = p1[i].GetComponent<Renderer>();
            rend.enabled = true;
            rend.sharedMaterial = material[0];
        }
    }

    public void OnClickBlack_p1()
    {
        for (int i = 0; p1.Length > i; i++)
        {
            rend = p1[i].GetComponent<Renderer>();
            rend.enabled = true;
            rend.sharedMaterial = material[1];
        }
    }
    public void OnClickBlue_p1()
    {
        for (int i = 0; p1.Length > i; i++)
        {
            rend = p1[i].GetComponent<Renderer>();
            rend.enabled = true;
            rend.sharedMaterial = material[2];
        }
    }
    public void OnClickRed_p1()
    {
        for (int i = 0; p1.Length > i; i++)
        {
            rend = p1[i].GetComponent<Renderer>();
            rend.enabled = true;
            rend.sharedMaterial = material[3];
        }
    }
    public void OnClickWhite_p1()
    {
        for (int i = 0; p1.Length > i; i++)
        {
            rend = p1[i].GetComponent<Renderer>();
            rend.enabled = true;
            rend.sharedMaterial = material[4];
        }
    }
    /*---------------P2--------------------*/
    public void OnClickOrange_p2()
    {
        for (int i = 0; p2.Length > i; i++)
        {
            rend = p2[i].GetComponent<Renderer>();
            rend.enabled = true;
            rend.sharedMaterial = material[0];
        }
    }

    public void OnClickBlack_p2()
    {
        for (int i = 0; p2.Length > i; i++)
        {
            rend = p2[i].GetComponent<Renderer>();
            rend.enabled = true;
            rend.sharedMaterial = material[1];
        }
    }
    public void OnClickBlue_p2()
    {
        for (int i = 0; p2.Length > i; i++)
        {
            rend = p2[i].GetComponent<Renderer>();
            rend.enabled = true;
            rend.sharedMaterial = material[2];
        }
    }
    public void OnClickRed_p2()
    {
        for (int i = 0; p2.Length > i; i++)
        {
            rend = p2[i].GetComponent<Renderer>();
            rend.enabled = true;
            rend.sharedMaterial = material[3];
        }
    }
    public void OnClickWhite_p2()
    {
        for (int i = 0; p2.Length > i; i++)
        {
            rend = p2[i].GetComponent<Renderer>();
            rend.enabled = true;
            rend.sharedMaterial = material[4];
        }
    }

    public void Resume()
    {
        if (ToggleP1.isOn && ToggleP2.isOn)
        {
            Time.timeScale = 1f;
            GameScoreUI.GetComponent<Canvas>().enabled = true;
            ChangeColorUI.SetActive(false);
        }
    }
}
