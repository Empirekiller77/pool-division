﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class countDownTextController : MonoBehaviour
{
    public AudioClip timerClip;
    bool keepPlaying = true;
    float timeLeft = 5.0f;

    public TMPro.TextMeshProUGUI counter_text;
    public AudioClip[] audioClips = new AudioClip[2];
    private AudioSource audioSource;
    private bool gamestarted = false;
    // Start is called before the first frame update
    void Start()
    {
        counter_text = GetComponent<TMPro.TextMeshProUGUI>();
        this.audioSource = this.GetComponent<AudioSource>();
        timerClip = audioClips[0];
       
    }

    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime;
        

        if (timeLeft < 1 && timeLeft > -1 && !gamestarted)
        {
        
            audioSource.clip = audioClips[1];
            audioSource.Play();
            Wait(0.4f, () =>
            {
                timeLeft = -2;
                player_ball_identifier.disableBallTypeTexts();
        
                counter_text.text = "START!";
                
                Wait(1f, () =>
                {
                    counter_text.enabled = false;
                    gamestarted = true;
                });
                   

            });   
        }
        else if(timeLeft >= 1)
        {  
            counter_text.text = "" + Mathf.Round(timeLeft);
        }
    }

    public void Wait(float seconds, Action action)
    {  
       StartCoroutine(_wait(seconds, action));       
    }

    internal IEnumerator _wait(float time, Action callback)
    {
        yield return new WaitForSeconds(time);
        callback();
    }

    IEnumerator SoundOut()
    {   
        while (keepPlaying)
        {   
            audioSource.PlayOneShot(timerClip);
            Debug.Log("ChOO-ChOO");
            yield return new WaitForSeconds(1f);
        }
    }

    void PlaySound()
    {
        audioSource.PlayOneShot(timerClip);
    }
}
