﻿using UnityEngine;

public class hole_sound_controller : MonoBehaviour
{
    // Start is called before the first frame update
    private AudioSource holeScourse;

    internal void Start()
    {
        this.holeScourse = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    internal void Update()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("BallFilter"))
        {
            //ball entering hole clip
            holeScourse.Play();
        }
    }
}
