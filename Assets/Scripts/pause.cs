﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pause : MonoBehaviour
{
    public static bool GameIsPause = false;
    public GameObject pauseMenuUI;
    public GameObject inGameUI;
    private TMPro.TextMeshProUGUI canvas_middle_text;
    private GameObject ChangeColorUI;


    private void Start()
    {
        ChangeColorUI = GameObject.Find("Canvas Change Color");
        //canvas_middle_text = GameObject.Find("CountDownTimer").GetComponent<TMPro.TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ChangeColorUI.activeSelf == false)
        {

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (GameIsPause)
                {
                    ResumeGame();
                }
                else
                {
                    PauseGame();
                }
            }
        }
    }
    void PauseGame()
    {
        AudioListener.pause = true;
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPause = true;
        inGameUI.SetActive(false);
    }

    public void ResumeGame()
    {
        AudioListener.pause = false;
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPause = false;
        inGameUI.SetActive(true);
    }

    public void LoadMenu()
    {
        AudioListener.pause = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menus/menu");
        Debug.Log("Loading Menu...");
    }

    public void QuitGame()
    {
        Debug.Log("Quitting Game...");
        Application.Quit();
    }
}
