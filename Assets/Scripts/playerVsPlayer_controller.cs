﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerVsPlayer_controller : MonoBehaviour
{
    // Start is called before the first frame update
    static AudioSource playerScource;
    private AudioClip trimmedClip;

    void Start()
    {
        playerScource = gameObject.GetComponent<AudioSource>();
        //set car vs car hit clip
        trimmedClip = MakeSubclip(playerScource.clip, 0, 1);
        playerScource.clip = trimmedClip;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player") { 
        Debug.Log("HITTING PLAYER");
        //car hitting car
        playerScource.Play();
        }
        else
        {
            Debug.Log(collision.gameObject.tag);
        }

    }


    private AudioClip MakeSubclip(AudioClip clip, float start, float stop)
    {
        /* Create a new audio clip */
        int frequency = clip.frequency;
        float timeLength = stop - start;
        int samplesLength = (int)(frequency * timeLength);
        AudioClip newClip = AudioClip.Create(clip.name + "-sub", samplesLength, 1, frequency, false);
        /* Create a temporary buffer for the samples */
        float[] data = new float[samplesLength];
        /* Get the data from the original clip */
        clip.GetData(data, (int)(frequency * start));
        /* Transfer the data to the new clip */
        newClip.SetData(data, 0);
        /* Return the sub clip */
        return newClip;
    }
}
