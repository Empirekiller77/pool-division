﻿using UnityEngine;

public class player_ball_identifier : MonoBehaviour
{
    private string[] ball_types = new string[] { "You got the\n STRIPPED BALLS!", "You got the\n SOLID BALLS!" };

    private string BALL_TYPE = null;

    public static string ball_type_p1;

    public static string ball_type_p2;

    internal static GameObject p1txt;

    internal static GameObject p2txt;

    // Start is called before the first frame update
    internal void Start()
    {
        p1txt = GameObject.Find("Win_LossText_p1");
        p2txt = GameObject.Find("Win_LossText_p2");
        int index = Random.Range(0, 9);
        if (index % 2 == 0)
        {
            BALL_TYPE = ball_types[1];
        }
        else
        {
            BALL_TYPE = ball_types[0];
        }

        ball_type_p1 = BALL_TYPE;

        if (ball_type_p1 == ball_types[1])
        {
            ball_type_p2 = ball_types[0];
        }
        else
        {
            ball_type_p2 = ball_types[1];
        }

        p1txt.GetComponent<TMPro.TextMeshProUGUI>().text = ball_type_p1;
        p2txt.GetComponent<TMPro.TextMeshProUGUI>().text = ball_type_p2;
    }

    // Update is called once per frame
    internal void Update()
    {

    }

    public static void disableBallTypeTexts()
    {
        p1txt.SetActive(false);
        p2txt.SetActive(false);
    }
}
