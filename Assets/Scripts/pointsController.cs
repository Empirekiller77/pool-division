﻿using System;
using System.Collections;
using UnityEngine;

public class pointsController : MonoBehaviour
{
    public static int ballsLeftP1;

    public static int ballsLeftP2;

    public static Vector3 blackBallPos;

    public TMPro.TextMeshProUGUI textScore_p1;

    public TMPro.TextMeshProUGUI textScore_p2;

    // Start is called before the first frame update
    internal void Start()
    {
        ballsLeftP1 = 7;
        ballsLeftP2 = 7;
        textScore_p1 = GameObject.Find("points_player1").GetComponent<TMPro.TextMeshProUGUI>();
        textScore_p2 = GameObject.Find("points_player2").GetComponent<TMPro.TextMeshProUGUI>();
        setScoreText(ballsLeftP1, textScore_p1);
        setScoreText(ballsLeftP2, textScore_p2);
        blackBallPos = GameObject.Find("8").transform.position;
    }

    // Update is called once per frame
    internal void Update()
    {
    }

    internal void OnTriggerEnter(Collider other)
    {

        GameObject triggered_last_toTouch = other.gameObject.GetComponent<pool_ball_Controller>().getLast_player_to_touch();
        Wait(0.3f, () =>
        { //waits 1 second

            //check if last player to touch ball was p1
            if (triggered_last_toTouch == GameObject.Find("Player1"))
            {
                if (other.gameObject.tag == "8ball" && ballsLeftP1 == 0)
                {
                    other.gameObject.SetActive(false);
                    setWinText(textScore_p1);
                  
                }
                else
                {
                    respawnBall(other.gameObject);
                }

                if (other.gameObject.tag != "8ball")
                {
                    if ((other.gameObject.tag == "strippedBall" && player_ball_identifier.ball_type_p1 == "stripped")
                           || (other.gameObject.tag == "solidBall" && player_ball_identifier.ball_type_p1 == "solid"))
                    {
                        oneBallLessP1(textScore_p1);
                    }
                    else
                    {
                        oneBallLessP2(textScore_p2);
                    }
                    other.gameObject.SetActive(false);
                }
            }

            //check if last player to touch ball was p2
            if (triggered_last_toTouch == GameObject.Find("Player2"))
            {

                if (other.gameObject.tag == "8ball" && ballsLeftP2 == 0)
                {
                    other.gameObject.SetActive(false);
                    setWinText(textScore_p2);
                }
                else
                {
                    respawnBall(other.gameObject);
                }

                if (other.gameObject.tag != "8ball")
                {
                    if ((other.gameObject.tag == "strippedBall" && player_ball_identifier.ball_type_p2 == "stripped")
                       || (other.gameObject.tag == "solidBall" && player_ball_identifier.ball_type_p2 == "solid"))
                    {
                        oneBallLessP2(textScore_p2);
                        other.gameObject.SetActive(false);
                    }
                    else
                    {
                        oneBallLessP1(textScore_p1);
                        other.gameObject.SetActive(false);
                    }
                }       
            }

        });
    }

    internal void oneBallLessP1(TMPro.TextMeshProUGUI text)
    {
        ballsLeftP1--;
        setScoreText(ballsLeftP1, text);
    }

    internal void oneBallLessP2(TMPro.TextMeshProUGUI text)
    {
        ballsLeftP2--;
        setScoreText(ballsLeftP2, text);
    }

    public void setScoreText(int score, TMPro.TextMeshProUGUI text)
    {
        text.text = "Balls Left: " + score;
    }

    public void setWinText(TMPro.TextMeshProUGUI text)
    {
        text.text = "YOU WIN!";
        Time.timeScale = 0f;
    }

    public void respawnBall(GameObject ball)
    {
        ball.transform.position = blackBallPos;
    }

    public void Wait(float seconds, Action action)
    {
        StartCoroutine(_wait(seconds, action));
    }

    internal IEnumerator _wait(float time, Action callback)
    {
        yield return new WaitForSeconds(time);
        callback();
    }
}
