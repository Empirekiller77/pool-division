﻿using UnityEngine;

public class pool_ball_Controller : MonoBehaviour
{
    private GameObject last_play_to_touch;

    // Start is called before the first frame update
    internal void Start()
    {
        last_play_to_touch = null;
    }

    // Update is called once per frame
    internal void Update()
    {
    }

    internal void OnCollisionEnter(Collision collision)
    {
        Rigidbody rb = this.GetComponent<Rigidbody>();
        Vector3 vel = rb.velocity;
        if (collision.gameObject.tag == "Player")
        {
            last_play_to_touch = collision.gameObject;
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("BallFilter"))
        {
            if (collision.gameObject.GetComponent<pool_ball_Controller>()
                 .getLast_player_to_touch() != null)
            {
                //if (collision.rigidbody.velocity.magnitude> vel.magnitude)  {
                last_play_to_touch = collision.gameObject.GetComponent<pool_ball_Controller>()
                 .getLast_player_to_touch();

                // }

            }

        }
    }

    public GameObject getLast_player_to_touch()
    {
        return this.last_play_to_touch;
    }
}
