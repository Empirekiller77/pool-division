﻿using UnityEngine;
using UnityEngine.UI;

public class volume_start_controller : MonoBehaviour
{
    private Slider volumeSlider;

    // Start is called before the first frame update
    internal void Start()
    {
        volumeSlider = GetComponent<Slider>();
        if (volumeSlider.value == 0f)
        {         
            volumeSlider.value = 0.5f;
        }
    }
       
}
