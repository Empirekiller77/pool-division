using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof (CarController))]
    public class CarUserControl : MonoBehaviour
    {
        private CarController m_Car; // the car controller we want to use
        private TMPro.TextMeshProUGUI canvas_middle_text;
        private void Awake()
        {
            // get the car controller
            m_Car = GetComponent<CarController>();
            canvas_middle_text = GameObject.Find("CountDownTimer").GetComponent<TMPro.TextMeshProUGUI>();
        }


        private void FixedUpdate()
        {
            // pass the input to the car!
            float h = 0;
            float v = 0;
#if !MOBILE_INPUT

            //float handbrake = CrossPlatformInputManager.GetAxis("Jump");
            //no handbreak, maybe in the future?
            float handbrake = 0;
#else
            m_Car.Move(h, v, v, 0f);
#endif
            if (canvas_middle_text.enabled == false || canvas_middle_text.text == "START!")
            {
                if (this.name == "Player1")
                {
                    h = CrossPlatformInputManager.GetAxis("HorizontalWASD");
                    v = CrossPlatformInputManager.GetAxis("VerticalWASD");
                }
                else
                {
                    h = CrossPlatformInputManager.GetAxis("Horizontal");
                    v = CrossPlatformInputManager.GetAxis("Vertical");
                }
                m_Car.Move(h, v, v, handbrake);
            }
           
        }
    }
}
